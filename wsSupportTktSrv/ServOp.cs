﻿using DocumentFormat.OpenXml.Office2010.ExcelAc;
using EMS.Lib;
using gen.lib;
using gen.Lib;
using gen.LIb.Extended;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace wsSupportTktSrv
{
    public class ServOp
    {
        static int ReapatAfter = 1440; //minutes
        static DateTime initTime1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 11, 30, 0);
        static DateTime initTime2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 00, 0);

        public void Request()
        {
            DateTime now = DateTime.Now;
            //initTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 13, 49, 0);
            logger log = new logger();
            log.logInfo("Next Call 1 - Ticketing System@ : " + initTime2.ToString("dd-MMM-yyyy HH:mm"));
            log.logInfo("Request now 1  " + now.ToString() + " and init time " + initTime2);
            if (now >= initTime2)
            {
                log.logInfo("..Step One..");
                initTime2 = initTime2.AddMinutes(ReapatAfter);
                Step_2();               
            }
            log.logInfo("Next Call 2 - Ticketing System@ : " + initTime1.ToString("dd-MMM-yyyy HH:mm"));
            log.logInfo("Request now 2 " + now.ToString()+" and init time "+ initTime1);            
            if (now >= initTime1)
            {
                log.logInfo("..Step Two..");
                initTime1 = initTime1.AddMinutes(ReapatAfter);
                Step_1();

            }
            
        }


        public class cls_zonemgrs
        {
            public  int zone { get; set; }
            public string z_mngr { get; set; }
            public string email { get; set; }

        }

        public class cls_zonemgrs_summry
        {

            public int total_tickets { get; set; }
            public int total_closed { get; set; }
            public int total_pending { get; set; }
            public string zone_nm { get; set; }

        }

        public class cls_maildata
        {          
            public int m_status { get; set; }
          
        }
       

        public void Step_1()
        {
            logger log = new logger();

            try
            {
                clsDBWrapV2 db = new clsDBWrapV2();
                string output = db.executeSQLEx("Execute sp_GetJonhDeereZMngr");
                Resultset objset;
                JavaScriptSerializer j = new JavaScriptSerializer();
                objset = j.Deserialize<Resultset>(output);
                List<cls_zonemgrs> data = j.Deserialize<List<cls_zonemgrs>>(objset.rows);               

                output =db.executeSQLEx("Execute sp_GetTicketMailDetails 1");
                DataTable dt = db.ldset.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    int status = Convert.ToInt32(dt.Rows[0]["m_status"].ToString());               
                    if (status != 1)
                    {
                        TicketDetailMail(data);                       
                    }
                }
                else
                {
                    TicketDetailMail(data);
                }
            }
            catch (Exception e) {
                log.logError("Error : " + e.Message);
                //Helper.WitreErrorLog(e);
                //throw e;
            }
        }

        public void Step_2()
        {
            logger log = new logger();

            try
            {
                clsDBWrapV2 db = new clsDBWrapV2();
                string output = db.executeSQL("execute sp_getticketmaildetails 2", "");
                DataTable dt = db.ldset.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    int status = Convert.ToInt32(dt.Rows[0]["m_status"].ToString());
                    if (status != 1)
                    {
                        TicketDetailRptMail();
                    }
                }
                else
                {
                    TicketDetailRptMail();
                }
            }
            catch (Exception e)
            {
                log.logError("Error : " + e.Message);
            }
        }


        private void TicketDetailMail(List<cls_zonemgrs> data)
        {
            logger log = new logger();
            try
            {
                clsDBWrapV2 db = new clsDBWrapV2();
                Resultset objset;
                JavaScriptSerializer j = new JavaScriptSerializer();
                foreach (var o in data)
                {
                    string today = DateTime.Now.ToString("dd-MMM-yyyy");
                    string output = db.executeSQL("EXECUTE SprGetTicketDetailsByZone " + o.zone, "");
                    string fieldlist = "Ticket_Id|Project_Name|Call_Handled_by|Call_Closed_by|From|DealerJD|Dealership_Name|Case_Type|Email_subject|Status|Source|Source_Date_and_Time|Response_Time|Resolution_Provided_on|Resolved_In_Days|Reason_for_Delay|Final_Status|Mail_To|Mail_Cc|Remark1|Remark2|Remark3|Division";

                    string fname = "Td_Z" + o.zone + "_" + DateTime.Today.AddDays(-1).ToString("dd-MMM-yyyy");
                    string link = db.getExcelVirualPathfromDS(fname, fieldlist, "filename");

                    output = db.executeSQL("EXECUTE SprGetTicketDetailsByZoneSummary " + o.zone, "");

                    j = new JavaScriptSerializer();
                    objset = j.Deserialize<Resultset>(output);
                    List<cls_zonemgrs_summry> data_summ = j.Deserialize<List<cls_zonemgrs_summry>>(objset.rows);

                    string result = DateTime.Today.AddDays(-1).ToString("dd-MMM-yyyy");

                    emailSender email = new emailSender();
                    string body = "Dear Sir, <br/><br/>";
                    body += "Please find the below link to download the ticket statistics for <b>" + result + "</b>.<br/>";
                    //body += " Please find link for ticket detail " + link+ " For zone <b> "+data_summ[0].zone_nm+"</b><br/><br/>";
                    body += link + "<br/><br/>";
                    body += "Zone Name - <b>" + data_summ[0].zone_nm + "</b><br/>";
                    body += "Summary till <b>" + result + "</b><br/>";
                    body += "Total Support tickets <b>" + data_summ[0].total_tickets + " </b> (As on Date) <br/> ";
                    body += "Total Closed Support <b>" + data_summ[0].total_closed + "</b> (As on Date) <br/> ";
                    body += "Total Open Support <b>" + data_summ[0].total_pending + "</b> (As on Date) <br/><br/> ";
                    body += "Thank you & Regard,<br/>";
                    body += "Support Team";
                    email.sendEmail(o.email, "Support Ticket " + result + " report for " + data_summ[0].zone_nm + " Zone", body, "");


                }
                db.executeSQLEx("EXECUTE sp_TicketDetailsMail 1,1","");
            }
            catch (Exception e)
            {
                log.logError("Error : " + e.Message);
                //Helper.WitreErrorLog(e);
                //throw e;
            }
        }

        private void TicketDetailRptMail()
        {
            logger log = new logger();
            try
            {
                clsDBWrapV2 db = new clsDBWrapV2();
                JavaScriptSerializer j = new JavaScriptSerializer();

                string today = DateTime.Now.ToString("dd-MMM-yyyy");
                string output = db.executeSQLEx("EXECUTE sp_DailyTicketMailDetailsReport");
                string fieldlist = "Ticket_Id|Project_Name|Source_Date_and_Time|Email_subject|From|DealerJD|Dealership_Name|Status|Call_Handled_by|Reason_for_Delay|Email_To|Source|Response_Time|Mail_Cc|Case_Type|Final_Status|Resolution_Provided_on|Call_Closed_by|Resolved_In_Days|Remark1|Remark2|Remark3|Division";

                string fname = "Support_Ticket_Report-" + DateTime.Today.AddDays(-1).ToString("dd-MMM-yyyy");
                string link = db.getExcelVirualPathfromDS(fname, fieldlist, "filename");
                string result = DateTime.Today.AddDays(-1).ToString("dd-MMM-yyyy");
                
                emailSender email = new emailSender();
                string body = "Dear Sir, <br/><br/>";
                body += "Please find the below link to download the ticket statistics containing open tickets data till ";
                body += "<b>" + result + "</b> and closed daily tickets data of <b>" + result + " </b>.<br/> ";
                body += link + "<br/><br/>";
                body += "Thank you & Regard,<br/>";
                body += "Support Team";
                string subject = "Support Ticket Details - " + result;
                email.sendRptEmail(subject, body);

                db.executeSQLEx("EXECUTE sp_TicketDetailsMail 1,2", "");
            }
            catch (Exception e)
            {
                log.logError("Error : " + e.Message);
                //Helper.WitreErrorLog(e);
                //throw e;
            }
        }
    }
}
