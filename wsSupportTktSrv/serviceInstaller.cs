﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace wsSupportTktSrv
{
    [RunInstaller(true)]
    public partial class serviceInstaller : System.Configuration.Install.Installer
    {

            public serviceInstaller()
            {

                ServiceProcessInstaller serviceProcessInstaller = new ServiceProcessInstaller();
                ServiceInstaller serviceInstaller = new ServiceInstaller();

                //# Service Account Information
                serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
                serviceProcessInstaller.Username = null;
                serviceProcessInstaller.Password = null;

                //# Service Information
                serviceInstaller.DisplayName = "Service Ticket - Email";
                serviceInstaller.StartType = ServiceStartMode.Automatic;
                serviceInstaller.Description = "For sending ticket emails";

                //# This must be identical to the WindowsService.ServiceBase name
                //# set in the constructor of WindowsService.cs
                serviceInstaller.ServiceName = "msc_srv_tkt";

                this.Installers.Add(serviceProcessInstaller);
                this.Installers.Add(serviceInstaller);

                serviceInstaller.AfterInstall += ServiceInstaller_AfterInstall;
            }

            private void ServiceInstaller_AfterInstall(object sender, InstallEventArgs e)
            {
                ServiceController sc = new ServiceController("msc_srv_tkt");
                sc.Start();
            }


    }
}
