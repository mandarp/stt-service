﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.ServiceProcess;
using System.Timers;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using gen.Lib;

namespace wsSupportTktSrv
{
    public partial class winsrv : ServiceBase
    {
        Timer schedular = null;
        Boolean busy = false;
        int counter = 0;
       
       
        void initEvent()
        {

            logger log = new logger();
            if (schedular == null)
            {
                log.logInfo("SCheular created");
                schedular = new Timer(1000);
            }
            schedular.Elapsed += SchedularCallback;
            log.logInfo("Service Init");
            //schedular.Start();
            //if (!EventLog.SourceExists(sSource)) EventLog.CreateEventSource(sSource, sLog);
            busy = false;
        }

        private void SchedularCallback(object sender, ElapsedEventArgs e)
        {
            logger log = new logger();
            if (busy)
            {

                log.logInfo("In loop - Buzy request rejected " + counter);                //    EventLog.WriteEntry(sSource, "In loop - Buzy request rejected " + counter, EventLogEntryType.Information, 234);

            }
            else
            {
                busy = true;
                ServOp op = new ServOp();

                op.Request();
                busy = false;
            }
                counter++;
                return;
            

        }

        public winsrv()
        {
            busy = false;
            initEvent();
            logger log = new logger();
            log.logInfo("In contructor");
            this.CanHandlePowerEvent = true;
            this.CanHandleSessionChangeEvent = true;
            this.CanPauseAndContinue = true;
            this.CanShutdown = true;
            this.CanStop = true;
        }
        // Events
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        protected override void OnStart(string[] args)
        {

            base.OnStart(args);
            logger log = new logger();
            log.logInfo("In Start");

            try
            {
                schedular.Start();
            }
            catch (Exception e)

            {

                log.logError("Failed to start timer "+e.Message);

            }
            busy = false;
        }

        protected override void OnStop()
        {
            base.OnStop();
            schedular.Stop();
            schedular.Dispose();
            busy = false;
            logger log = new logger();

            log.logInfo("In Stop");
        }


        protected override void OnPause()
        {
            base.OnPause();
            logger log = new logger();
            log.logInfo("In Pause");

        }
        protected override void OnContinue()
        {
            base.OnContinue();
            logger log = new logger();
            log.logInfo("In Continue");

        }
        protected override void OnShutdown()
        {
            base.OnShutdown();
            schedular.Stop();
            schedular.Dispose();
        }
        protected override void OnCustomCommand(int command)
        {
            //  A custom command can be sent to a service by using this method:
            //#  int command = 128; //Some Arbitrary number between 128 & 256
            //#  ServiceController sc = new ServiceController("NameOfService");
            //#  sc.ExecuteCommand(command);

            base.OnCustomCommand(command);
        }
        protected override bool OnPowerEvent(PowerBroadcastStatus powerStatus)
        {
            return base.OnPowerEvent(powerStatus);
        }
        protected override void OnSessionChange(SessionChangeDescription changeDescription)
        {
            base.OnSessionChange(changeDescription);
        }

        static void Main()
        {
            ServiceBase.Run(new winsrv());

            //ServOp op = new ServOp();
            //op.Request();
            //op.Step_1();

        }

    }
}
