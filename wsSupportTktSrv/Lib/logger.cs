﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace gen.Lib
{
    class logger
    {
        public int mode { get; set; }
        public string sSource { get; set; }
        public string sLog { get; set; }
        public string sEvent { get; set; }
        public string sLocaLogPath { get; set; }

        public logger()
        {
            mode = 1;
            sSource = "Windows Service :: Ticket Emailer";
            sLog = "Application";
            sEvent = "Event Watch";
            sLocaLogPath = "c:\\temp\\mssql_backup.log";
            if (mode == 1)
            {
                if (!EventLog.SourceExists(sSource)) EventLog.CreateEventSource(sSource, sLog);
            }
        }
        public void logit(string msg, EventLogEntryType etype)
        {
            if (mode == 1)
            {
                EventLog.WriteEntry(sSource, msg, etype, 622);
            }
        }

        public void logInfo(string msg) { logit(msg, EventLogEntryType.Information); }
        public void logWarn(string msg) { logit(msg, EventLogEntryType.Warning); }
        public void logError(string msg) { logit(msg, EventLogEntryType.Error); }

    }

    // New XML Logger
    public class xmlLogger
    {
        public string lastError = "";
        public int lastErrorCode = 0;
        public string rootpath = "c:\\MsService\\";
        public xmlLogger() { createLog(); }
        public string GetParamFromXml(string node)
        {
            try
            {
                var doc = new XmlDocument();

                doc.Load(rootpath + "param.xml");
                return doc.GetElementsByTagName(node)[0].InnerText.Replace(@"\r\n", "").Trim().ToString();

            }
            catch (Exception e) { lastError = e.Message.ToString(); return ""; }
        }
        public void SetParamFromXml(string node, String newval)
        {
            try
            {
                var doc = new XmlDocument();

                doc.Load(rootpath + "param.xml");
                string lastval = doc.GetElementsByTagName(node)[0].InnerText.Replace(@"\r\n", "").Trim().ToString();
                doc.GetElementsByTagName(node)[0].InnerText = newval;
                doc.Save(rootpath + "param.xml");

            }
            catch (Exception e) { lastError = e.Message.ToString(); return; }
        }



        public void createLog()
        {
            string output = "<params><mainemail> </mainemail><ccline></ccline></params>";
            string path = rootpath + "param.xml";
            if (!File.Exists(path)) { File.WriteAllText(path, output); }
            string curdir = Directory.GetCurrentDirectory();
        }





    }

    public class txtLog
    {
        public string m_FilePath;
        public txtLog(string p_FilePath) { m_FilePath = p_FilePath; }
        public void delete()
        {
            try
            {
                if (File.Exists(m_FilePath)) {  File.Delete(m_FilePath); }
            }
            catch (Exception e)
            {

            }


        }


        public void write(string p_msg)
        {
            try
            {
                if (!File.Exists(m_FilePath)) { var a = File.Create(m_FilePath); a.Close(); }
                string fmsg = p_msg + " @ " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss") + " <br/>\r\n";
                File.AppendAllText(m_FilePath, fmsg);
            }
            catch (Exception e)
            {

            }


        }


        public void writep(string p_msg)
        {
            try
            {
                if (!File.Exists(m_FilePath)) { var a = File.Create(m_FilePath); a.Close(); }
                string fmsg = p_msg + "\n";
                File.AppendAllText(m_FilePath, fmsg);
            }
            catch (Exception e)
            {

            }

        }

        public string read()
        {
            string ret = "";
            try
            {
                if (File.Exists(m_FilePath)) { ret = File.ReadAllText(m_FilePath); }
                
            }
            catch (Exception e)
            {

            }
            return ret;

        }

    }
}