﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Diagnostics;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using gen.Lib;
using gen.LIb.Extended;
using EMS.Lib;
using System.Web.Script.Serialization;
using wsSupportTktSrv;
using System.Data;

namespace gen.lib
{
    public class emailSender
    {

        public String eventSrc = "MS Alert Service";
        public int eventId = 244;
        public string ccaddress = "";

        public emailSender() { }
        public class cls_ccrecipents
        {
            public string email { get; set; }
        }
        public class cls_torecipents
        {
            public string email { get; set; }
        }

        public void sendEmail(String p_receipent, String p_subject, String p_body, String p_file)
        {
            logger log = new logger();
            log.mode = 2;
            var fromAddress = new MailAddress("emssupport@mindspacetech.com", "Support Team");
            var toAddress = new MailAddress(p_receipent, p_receipent);
            const string fromPassword = "cisco@disco1234";
            String subject = p_subject;
            String body = p_body;
            
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };

            using (var message = new MailMessage(fromAddress, toAddress) { Subject = subject, Body = body, IsBodyHtml = true })
            {

                clsDBWrapV2 db = new clsDBWrapV2();
                string output = db.executeSQLEx("Execute sp_getCcEmail");
                Resultset objset;
                JavaScriptSerializer j = new JavaScriptSerializer();
                objset = j.Deserialize<Resultset>(output);

                 List<cls_ccrecipents> data = j.Deserialize<List<cls_ccrecipents>>(objset.rows);
                
                try
                {
                    foreach (var o in data)
                    {
                        message.CC.Add(o.email);
                        //message.CC.Add(data);

                    }
                   
                    if (!String.IsNullOrEmpty(p_file))
                    {
                        Attachment att = new Attachment(p_file);
                        message.Attachments.Add(att);
                    }
                    ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                    smtp.Send(message);

                    
                }
                catch (Exception e)
                {
                    log.logError("Error : " + e.Message);
                    //Helper.WitreErrorLog(e);
                    //throw e;
                }
            }
        }

        public void sendRptEmail(String p_subject, String p_body)
        {
            clsDBWrapV2 db = new clsDBWrapV2();
            logger log = new logger();
            log.mode = 2;
            var fromAddress = new MailAddress("emssupport@mindspacetech.com", "Support Team");
            const string fromPassword = "cisco@disco1234";
            String subject = p_subject;
            String body = p_body;
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage()
            {
                //fromAddress, toAddress                
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
            {
                try
                {
                    string output = db.executeSQLEx("Execute sp_DailyTicketMailDetails");
                    DataTable dt0 = db.ldset.Tables[0];
                    if (dt0.Rows.Count > 0)
                    {
                        string p_tomail = dt0.Rows[0]["m_mail"].ToString();
                        MailAddressCollection addressList = new MailAddressCollection();
                        foreach (var curr_address in p_tomail.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            MailAddress mytoAddress = new MailAddress(curr_address, curr_address);
                            addressList.Add(mytoAddress);
                        }
                        message.To.Add(addressList.ToString());
                    }

                    //var toAddress = new MailAddress(tm, "To Name");
                    DataTable dt1 = db.ldset.Tables[1];
                    if (dt1.Rows.Count > 0)
                    {
                        string p_ccmail = dt1.Rows[0]["m_mail"].ToString();
                        MailAddressCollection addressList = new MailAddressCollection();
                        foreach (var curr_address in p_ccmail.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            MailAddress mytoAddress = new MailAddress(curr_address, curr_address);
                            addressList.Add(mytoAddress);
                        }
                        message.CC.Add(addressList.ToString());

                    }

                    message.From = fromAddress;
                    ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                    smtp.Send(message);
                }
                catch (Exception e)
                {
                    log.logError("Error : " + e.Message);
                }
            }
        }

        public void testEmail()
        {
            var fromAddress = new MailAddress("emssupport@mindspacetech.com", "Support Team");
            var toAddress = new MailAddress("vishwanath.b@mindspacetech.com", "To Name");
            const string fromPassword = "cisco@disco1234";
            const string subject = "Subject";
            const string body = "Body";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                try
                {
                    Attachment att = new Attachment("C:\\myfile.pdf");
                    message.Attachments.Add(att);

                    smtp.Send(message);
                }
                catch (Exception e)
                {
                    logger log = new logger();
                    log.logError("IN Test email : Error > " + e.Message);

                }
            }
        }

       

    }
}
