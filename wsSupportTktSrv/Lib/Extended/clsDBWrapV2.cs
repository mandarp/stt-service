﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using System.Text;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using System.Globalization;
using gen.Lib;
using System.Text.RegularExpressions;
using EMS.Lib;
using ems.Lib;

namespace gen.LIb.Extended
{

    public class ProcTableset
    {
        public int status { get; set; }
        public string msg { get; set; }
        public string sql { get; set; }
        public string reserved { get; set; }
        public List<string> rows { get; set; }

    }
    public class clsDBWrapV2 : clsDbWrapper
    { 
        public ProcTableset pts = null;
       
        public Resultset lrs;
        
        public string executeSQLEx(string p_SQL, string p_Reserved = null)
        {
            Resultset objres = new Resultset();
            if (pts != null) pts = null;
            pts = new ProcTableset();
            objres.sql = p_SQL;
            string retVal = string.Empty;
            int result = 0;
            CreateConn();

            try
            {
                if (p_SQL.IndexOf("Select", StringComparison.OrdinalIgnoreCase) > -1 || p_SQL.IndexOf("Execute", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    SqlDataAdapter da = new SqlDataAdapter();
                    SqlCommand cmd = con.CreateCommand();
                    cmd.CommandText = p_SQL;
                    cmd.CommandTimeout = 300;
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    ldset = ds;
                    string[] jsonArray = new string[ds.Tables[0].Columns.Count];
                    string headString = string.Empty;

                    for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                    {
                        jsonArray[i] = ds.Tables[0].Columns[i].Caption;
                        headString += "\"" + jsonArray[i] + "\" : \"" + jsonArray[i] + i.ToString() + "%" + "\",";
                    }

                    headString = headString.Substring(0, headString.Length - 1);
                    StringBuilder sb = new StringBuilder();
                    sb.Append("[");
                    objres.trows = ds.Tables[0].Rows.Count;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            string colsString = headString;
                            sb.Append("{");
                            for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                            {
                                String d = ds.Tables[0].Rows[i][j].ToString();
                                d = d.Replace("\\", "-");
                                d = d.Replace("\"", "\\\"");
                                d = d.Replace("\n", " ");
                                d = d.Replace("\r", " ");
                                d = getASCII_str(d);
                                colsString = colsString.Replace(ds.Tables[0].Columns[j] + j.ToString() + "%", d);
                            }
                            sb.Append(colsString + "},");
                        }
                    }
                    else
                    {
                        string colsString = headString;
                        sb.Append("{");
                        for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                        {
                            colsString = colsString.Replace(ds.Tables[0].Columns[j] + j.ToString() + "%", "");
                        }
                        sb.Append(colsString + "},");
                    }

                    sb = new StringBuilder(sb.ToString().Substring(0, sb.ToString().Length - 1));
                    sb.Append("]");

                    objres.status = 200;
                    objres.msg = "Ok";
                    objres.refcode = -1;
                    objres.rows = sb.ToString();

                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                    MemoryStream ms = new MemoryStream();
                    serializer.WriteObject(ms, objres);
                    lrs = objres;
                    retVal = Encoding.UTF8.GetString(ms.ToArray());
                    // Special treatment if second table with total records
                    if (p_SQL.IndexOf("Execute", StringComparison.OrdinalIgnoreCase) > -1)
                    {
                        // need reset objres.trows =
                        string strrecs = "";
                        if (ds.Tables.Count > 0)
                        {
                            strrecs = ds.Tables[0].Rows.Count.ToString();
                            /*  if (ds.Tables.Count > 1)
                              {
                                  try
                                  {
                                      strrecs = ds.Tables[1].Rows[0]["tot_rec"].ToString();
                                  }
                                  catch (Exception ex)
                                  {
                                      //objres.trows = null;
                                  }
                              }
                              */
                            if (ds.Tables.Count > 1)
                            {

                                objres.reserved = DSToJSONString(ds, 1);
                            }

                            // Also create procset
                            int ti = 0;
                            pts.msg = "Processing...";
                            pts.status = 0;
                            pts.rows = new List<string>();
                            foreach (var ts in ds.Tables)
                            {
                                string rows = DSToJSONString(ds, ti); ti++;
                                pts.rows.Add(rows);
                            }
                        }
                    }

                }
                //else if (p_SQL.IndexOf("Insert", StringComparison.OrdinalIgnoreCase) > -1)
                else if (Regex.IsMatch(p_SQL, "\\bINSERT\\b", RegexOptions.IgnoreCase))
                {
                    SqlCommand cmd = new SqlCommand(p_SQL, con);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = p_SQL;
                    if (p_SQL.IndexOf("SCOPE_IDENTITY", StringComparison.OrdinalIgnoreCase) > -1)
                    {
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            reader.Read();
                            result = Convert.ToInt32(reader[0]);
                        }
                        reader.Close();
                    }
                    else
                    {
                        result = cmd.ExecuteNonQuery();
                    }

                    objres.status = 200;
                    objres.msg = "Ok";
                    objres.refcode = result;
                    objres.rows = null;

                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                    MemoryStream ms = new MemoryStream();
                    serializer.WriteObject(ms, objres);
                    retVal = Encoding.UTF8.GetString(ms.ToArray());
                }
                else if (p_SQL.IndexOf("Update", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    SqlCommand cmd = new SqlCommand(p_SQL, con);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = p_SQL;
                    DataTable dt = new DataTable();
                    DataRow dr = null;
                    DataColumn dc = null;
                    StringBuilder sb = new StringBuilder();
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        //reader.Read();
                        dt.Load(reader);
                        result = dt.Rows.Count;
                        dc = new DataColumn("upd_cnt", typeof(System.String));
                        dc.DefaultValue = result.ToString();
                        dt.Columns.Add(dc);
                        dt.Columns["upd_cnt"].SetOrdinal(0);
                    }
                    else
                    {
                        result = reader.RecordsAffected;
                        dt.Columns.Add("upd_cnt", typeof(System.String));
                        if (Regex.IsMatch(p_SQL, "\\bOUTPUT INSERTED.\\b", RegexOptions.IgnoreCase) && result == -1)
                        {
                            dt.Columns.Add("m_sys_cd", typeof(System.String));
                            dr = dt.NewRow();
                            dr["upd_cnt"] = "";
                            dr["m_sys_cd"] = "";
                            dt.Rows.Add(dr);
                            result = 0;
                        }
                        else
                        {
                            dr = dt.NewRow();
                            dr["upd_cnt"] = result.ToString();
                            dt.Rows.Add(dr);
                        }
                    }

                    reader.Close();

                    //45hu_20180407_1105
                    string[] jsonArray = new string[dt.Columns.Count];
                    string headString = string.Empty;

                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        jsonArray[i] = dt.Columns[i].Caption;
                        headString += "\"" + jsonArray[i] + "\" : \"" + jsonArray[i] + i.ToString() + "%" + "\",";
                    }

                    headString = headString.Substring(0, headString.Length - 1);

                    sb.Append("[");
                    objres.trows = dt.Rows.Count;
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string colsString = headString;
                            sb.Append("{");
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                String d = dt.Rows[i][j].ToString();
                                d = d.Replace("\\", "-");
                                d = d.Replace("\"", "\\\"");
                                d = d.Replace("\n", " ");
                                d = d.Replace("\r", " ");
                                d = getASCII_str(d);
                                colsString = colsString.Replace(dt.Columns[j] + j.ToString() + "%", d);
                            }
                            sb.Append(colsString + "},");
                        }
                    }
                    else
                    {
                        string colsString = headString;
                        sb.Append("{");
                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            colsString = colsString.Replace(dt.Columns[j] + j.ToString() + "%", "");
                        }
                        sb.Append(colsString + "},");
                    }

                    sb = new StringBuilder(sb.ToString().Substring(0, sb.ToString().Length - 1));
                    sb.Append("]");
                    //45hu_20180407_1105

                    objres.status = 200;
                    objres.msg = "Ok";
                    objres.refcode = -1; // dt.Rows.Count == 1 ? Convert.ToInt32(dt.Rows[0][0].ToString()) : result;
                    objres.rows = sb.ToString();
                    objres.trows = result;

                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                    MemoryStream ms = new MemoryStream();
                    serializer.WriteObject(ms, objres);
                    retVal = Encoding.UTF8.GetString(ms.ToArray());
                }
                else if (p_SQL.IndexOf("Delete", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    SqlCommand cmd = new SqlCommand(p_SQL, con);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = p_SQL;

                    result = cmd.ExecuteNonQuery();

                    objres.status = 200;
                    objres.msg = "Ok";
                    objres.refcode = result;
                    objres.rows = null;

                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                    MemoryStream ms = new MemoryStream();
                    serializer.WriteObject(ms, objres);
                    retVal = Encoding.UTF8.GetString(ms.ToArray());
                }
                CloseConn();
            }
            catch (Exception ex)
            {
                objres.status = 0;
                objres.msg = ex.Message.ToString();
                objres.refcode = 0;
                objres.trows = 0;
                objres.rows = null;
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                MemoryStream ms = new MemoryStream();
                serializer.WriteObject(ms, objres);
                retVal = Encoding.UTF8.GetString(ms.ToArray());
            }

            return retVal;
        }



        String DSToJSONString(DataSet p_ds, int p_Ti)
        {
            string ret = "";


            string[] jsonArray = new string[p_ds.Tables[p_Ti].Columns.Count];
            string headString = string.Empty;

            for (int i = 0; i < p_ds.Tables[p_Ti].Columns.Count; i++)
            {
                jsonArray[i] = p_ds.Tables[p_Ti].Columns[i].Caption;
                // headString += "'" + jsonArray[i] + "' : '" + jsonArray[i] + i.ToString() + "%" + "',";
                headString += "\"" + jsonArray[i] + "\" : \"" + jsonArray[i] + i.ToString() + "%" + "\",";
            }

            headString = headString.Substring(0, headString.Length - 1);
            StringBuilder sb = new StringBuilder();
            sb.Append("[");

            if (p_ds.Tables[p_Ti].Rows.Count > 0)
            {
                for (int i = 0; i < p_ds.Tables[p_Ti].Rows.Count; i++)
                {
                    string colsString = headString;
                    sb.Append("{");
                    for (int j = 0; j < p_ds.Tables[p_Ti].Columns.Count; j++)
                    {
                        String d = p_ds.Tables[p_Ti].Rows[i][j].ToString();
                        d = d.Replace("\\", "-");
                        d = d.Replace("\"", "\\\"");
                        d = d.Replace("\n", " ");
                        d = d.Replace("\r", " ");

                        colsString = colsString.Replace(p_ds.Tables[p_Ti].Columns[j] + j.ToString() + "%", d);
                    }
                    sb.Append(colsString + "},");
                }
            }
            else
            {
                string colsString = headString;
                sb.Append("{");
                for (int j = 0; j < p_ds.Tables[p_Ti].Columns.Count; j++)
                {
                    colsString = colsString.Replace(p_ds.Tables[p_Ti].Columns[j] + j.ToString() + "%", "");
                }
                sb.Append(colsString + "},");
            }

            sb = new StringBuilder(sb.ToString().Substring(0, sb.ToString().Length - 1));
            sb.Append("]");


            ret = sb.ToString();




            return ret;



        }


        public string getInExcelFromDataSet(DataSet p_ds, string filenm, string fldlist, string sheetName, int table_index = 0, string addrows="")
        {
            string path_Root = @"C:\iiswww\globalResource\xls";
            string srcpath = "http://Localhost/Grscxls/";

            xmlLogger xlog = new xmlLogger();
            string t;
            t = xlog.GetParamFromXml("localXLPath");
            if (!String.IsNullOrEmpty(t)) path_Root = t;

            t = xlog.GetParamFromXml("ServerXlPath");
            if (!String.IsNullOrEmpty(t)) srcpath = t;

            string path_Random = System.IO.Path.GetRandomFileName();
            string dw_path = filenm;
            string filepath = string.Empty; string fileName = string.Empty;
            fileName = path_Root + "\\" + path_Random + "\\" + dw_path + ".xlsx";
            srcpath = "";// ConfigurationManager.AppSettings["ExportExcel_Path"].ToString();
            String serverpath = srcpath + "/" + path_Random + "/" + dw_path + ".xlsx";
            ClosedXLHelperEx cl = new ClosedXLHelperEx();
            string newfields = cl.setColInfo(fldlist);
            cl.AppendRow(newfields, fileName, sheetName);
            int Randno = 0;
            if (!String.IsNullOrEmpty(addrows))
            {
                foreach (var s1 in addrows.Split('~'))
                {
                    cl.AppendRow(s1, fileName, sheetName);
                }



                cl.AppendRow(newfields, fileName, sheetName);
            }

            try
            {
                cl.AppendDSAdv(p_ds, fileName, sheetName, table_index);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return serverpath;

        }

        public clsPaths getRandomFile(string p_filenm)
        {
            clsPaths ret = new clsPaths();
            string path_Root = @"C:\iiswww\globalResource\xls";
            string srcpath = "http://Localhost/Grscxls/";
            xmlLogger xlog = new xmlLogger();
            string t;
            t = xlog.GetParamFromXml("localXLPath");
            if (!String.IsNullOrEmpty(t)) path_Root = t;
            t = xlog.GetParamFromXml("ServerXlPath");
            if (!String.IsNullOrEmpty(t)) srcpath = t;
            string path_Random = System.IO.Path.GetRandomFileName();
            string dw_path = path_Root + "\\" + path_Random + "\\" + p_filenm + ".xlsx"; ;
            String serverpath = srcpath + path_Random + "/" + p_filenm + ".xlsx";

            ret.localpath = dw_path;
            ret.serverpath = serverpath;
            return ret;

        }

        

    }

    public class clsPaths {
        public string serverpath { get; set; }
        public string localpath { get; set; }

    }


}