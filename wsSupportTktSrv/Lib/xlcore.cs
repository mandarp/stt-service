﻿using EMS.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ems.Models.EMS
{

    public class clsListData
    {
        public int m_sys_cd { get; set; }
        public string m_Name { get; set; }
        public int m_MobileNo { get; set; }
        public int code { get; set; }
        public string name { get; set; }
        public int refcode { get; set; }

        public string str1 { get; set; }

        //public string emailid { get; set; }
        public int? sys_cd { get; set; }
        public string dse_name { get; set; }
        public string RACF_ID { get; set; }
        //public string name { get; set; }
        public string status { get; set; }
        public int? tot_enq { get; set; }
        public string m_vendor_cd { get; set; }
        public string m_contact_no { get; set; }
    }
    public class clsList
    {

        public int status { get; set; }
        public string msg { get; set; }
        public List<clsListData> data { get; set; }



    }
    public class clsReportData
    {
        public int status { get; set; }
        public string msg { get; set; }
        public int trn_rec { get; set; }
        public string data { get; set; }
        public int m_sys_cd { get; set; }

    }
    public class clsListDataenquire
    {
        public int code { get; set; }
        public string name { get; set; }
        public int refcode { get; set; }
        public string m_vendor_cd { get; set; }

        public string str1 { get; set; }
    }

    public class clsCGenListSpecific
    {
        public int code { get; set; }
        public string name { get; set; }
        public string refcode { get; set; }
        public string m_vendor_cd { get; set; }

        public string str1 { get; set; }
    }

    public class clsListenquire
    {

        public int status { get; set; }
        public string msg { get; set; }
        public List<clsListDataenquire> data { get; set; }
    }

    public class clsCGenSpecific
    {

        public int status { get; set; }
        public string msg { get; set; }
        public List<clsCGenListSpecific> data { get; set; }
    }

    public class clsenquiries
    {
        public string enqid { get; set; }
        public string dealername { get; set; }
        public string m_name { get; set; }
        public string f_item_id { get; set; }
        public string m_desc { get; set; }
        public int status { get; set; }
        public string msg { get; set; }
        public int trn_rec { get; set; }
        public string data { get; set; }
        public int f_sys_cd_dlr { get; set; }
        public string statusa { get; set; }
        public string descript { get; set; }
        public string f_tractor_reg_no { get; set; }
        public string regno { get; set; }
        public string enq_category { get; set; }
    }
    public class clsResp
    {
        public string info { get; set; }
        public int status { get; set; }
        public string msg { get; set; }
        public int refcode { get; set; }
        public string reserved { get; set; }

    }
    public class clsenquire
    {
        public int status { get; set; }
        public string msg { get; set; }
        public List<clsenquiredata> data { get; set; }
    }
    public class clsenquiredata
    {

        public long m_sys_cd { get; set; }
        public int ph_cd { get; set; }
        public DateTime m_enq_dt { get; set; }
        public int f_sys_cd_dse { get; set; }
        public int f_sys_cd_dlr { get; set; }
        public string f_sys_cd_cust { get; set; }
        public int f_enq_status { get; set; }
        public int f_enq_cat { get; set; }
        public int m_Landsize { get; set; }
        public int f_Crops { get; set; }
        public int f_sys_cd_prodappln { get; set; }
        public string m_Application { get; set; }
        public int f_sys_cd_model { get; set; }
        public int m_qty { get; set; }
        public int m_Offered_Price { get; set; }
        public string m_sub_enq_id { get; set; }
        public int m_CustExpected_Price { get; set; }
        public DateTime m_NextFollowUpOn { get; set; }
        public string m_Open_reason { get; set; }
        public DateTime m_DeliveryOn { get; set; }
        public int m_Delivery_price { get; set; }
        public int f_sys_cd_model_purchased { get; set; }
        public DateTime m_planned_BuyingOn { get; set; }
        public string m_serialNo { get; set; }
        public DateTime m_BuyingOn { get; set; }
        public string m_Booking_Reason { get; set; }
        public int m_BookingAmount { get; set; }
        public DateTime m_ProposedDeliveryOn { get; set; }
        public int m_FinalPrice { get; set; }
        public int m_PaymentMode { get; set; }
        public int m_LostReasonCd { get; set; }
        public int m_LostToCompanyCd { get; set; }
        public DateTime m_Deffer_till { get; set; }
        public string m_Deffer_reason { get; set; }
        public int f_sys_cd_cgenerator { get; set; }
        public int f_sys_cd_model_delv { get; set; }
        public DateTime m_orgptb_dt { get; set; }

    }
    

    public class clsColInfo
    {
        public string col_nm { get; set; }
        public string shw_col { get; set; }
        public string grp_on { get; set; }
        public string filtertxt { get; set; }
        public string fldnm { get; set; }
        public short grptag { get; set; }
        public string reserved { get; set; }


        public int col_type { get; set; }
        public int col_cat { get; set; }
        public int master_index { get; set; }

    }


    public class clsenquiryLog
    {
        public string m_enq_dt { get; set; }
        public string m_NextFollowUpOn { get; set; }
        public string m_planned_BuyingOn { get; set; }
        public string m_Open_reason { get; set; }

        public int status { get; set; }
        public string msg { get; set; }
        public int trn_rec { get; set; }
        public string data { get; set; }

    }
    public class clsTractorInventory
    {
        public string sys_cd { get; set; }
        public string year { get; set; }
        public string m_item_id { get; set; }
        public string m_reg_no { get; set; }
        public string m_serial_no { get; set; }
        public string m_expected_price { get; set; }
        public string f_sys_Cd_enq_no { get; set; }
        public string f_sys_cd_dlr { get; set; }
        public string m_desc { get; set; }
        public string m_dlr_nm { get; set; }
        public string f_sys_cd_company { get; set; }
    }


    public class clsEnqdata
    {
        public string enqid { get; set; }
        public string dealername { get; set; }
        public string m_name { get; set; }
        public string m_sys_cd { get; set; }
        public string cust_id { get; set; }
        public string m_Offered_Price { get; set; }
        public string f_tractor_reg_no { get; set; }
        public string m_CustExpected_Price { get; set; }
        public string f_sys_cd_enqsrc { get; set; }
        public string f_sys_cd_model { get; set; }
        public string productname { get; set; }
        public string f_sys_cd_dse { get; set; }
        public string f_sys_cd_cgenerator { get; set; }
        public string f_sys_cd_cgenerator2 { get; set; }
        public string f_sys_cd_cgenerator1 { get; set; }
        public string f_sys_cd_prodappln { get; set; }
        public string f_enq_status { get; set; }
        public string enqdate { get; set; }
        public string plantobuydate { get; set; }
        public string remark { get; set; }
        public string nextfollowup { get; set; }
        public string descript { get; set; }
        public string f_actv_cd { get; set; }
        public string f_actv_dt { get; set; }
        public string make_cd { get; set; }
        public string invt_cd { get; set; }
        public string m_model { get; set; }
        public string m_serial_no { get; set; }
        public string m_expected_price { get; set; }
        public string m_actualexchange_price { get; set; }
        public string m_exchange_dt { get; set; }
        public string m_mfg_year { get; set; }
        public string m_exchange_rc_no { get; set; }
        public string m_vendor_cd { get; set; }
        public string pin_no { get; set; }
        public string delv_actualprice { get; set; }
        public int f_enq_cat { get; set; }
        // public int chkradio { get; set; }
        public string FollowUpRemarks { get; set; }

        public int enq_status { get; set; }
        public string m_financer { get; set; }
        // public int JD_available { get; set; }




        public void logit(clsEnqdata psrc)
        {
            string sql = "";
            clsDbWrapper db = new clsDbWrapper();
            bool usrlog = false;

            if (psrc.enq_status != enq_status)
            {
                sql = "Insert into t_enquiry_change_log (m_field_name,m_old_value, m_new_value,f_sys_cd_enq,m_stamp_add,m_actv,m_rstat) ";
                sql += " values ('Enquiry Status','" + psrc.enq_status + "','" + enq_status + "'," + m_sys_cd.ToString() + ", getdate(),1,1);";
                db.executeSQL(sql, "");

            }
            if (psrc.enqdate != enqdate)
            {
                sql = "Insert into t_enquiry_change_log (m_field_name,m_old_value, m_new_value,f_sys_cd_enq,m_stamp_add,m_actv,m_rstat) ";
                sql += " values ('Enquiry date','" + psrc.enqdate + "','" + enqdate + "'," + m_sys_cd.ToString() + ", getdate(),1,1);";
                db.executeSQL(sql, "");
            }

            if (psrc.f_sys_cd_dse != f_sys_cd_dse)
            {
                string ov = db.singleDataString("Select m_display_name from m_appusers where m_sys_cd =" + psrc.f_sys_cd_dse.ToString());
                string nv = db.singleDataString("Select m_display_name from m_appusers where m_sys_cd =" + f_sys_cd_dse.ToString());
                sql = "Insert into t_enquiry_change_log (m_field_name,m_old_value, m_new_value,f_sys_cd_enq,m_stamp_add,m_actv,m_rstat) ";
                sql += " values ('DSE Name','" + ov + "','" + nv + "'," + m_sys_cd.ToString() + ", getdate(),1,1);";
                db.executeSQL(sql, "");

            }

            if (psrc.f_sys_cd_enqsrc != f_sys_cd_enqsrc)
            {
                string ov = db.singleDataString("select m_desc from m_EnquirySources where m_sys_cd=" + psrc.f_sys_cd_enqsrc.ToString());
                string nv = db.singleDataString("select m_desc from m_EnquirySources where m_sys_cd=" + f_sys_cd_enqsrc.ToString());
                sql = "Insert into t_enquiry_change_log (m_field_name,m_old_value, m_new_value,f_sys_cd_enq,m_stamp_add,m_actv,m_rstat) ";
                sql += " values ('Source of enquiry','" + ov + "','" + nv + "'," + m_sys_cd.ToString() + ", getdate(),1,1);";
                db.executeSQL(sql, "");

            }

            if (psrc.f_sys_cd_prodappln != f_sys_cd_prodappln)
            {
                string ov = db.singleDataString("select m_desc from m_ProdApplnList where m_sys_cd=" + psrc.f_sys_cd_prodappln.ToString());
                string nv = db.singleDataString("select m_desc from m_ProdApplnList where m_sys_cd=" + f_sys_cd_prodappln.ToString());
                sql = "Insert into t_enquiry_change_log (m_field_name,m_old_value, m_new_value,f_sys_cd_enq,m_stamp_add,m_actv,m_rstat) ";
                sql += " values ('Application ','" + ov + "','" + nv + "'," + m_sys_cd.ToString() + ", getdate(),1,1);";
                db.executeSQL(sql, "");

            }

            if (psrc.f_sys_cd_cgenerator != f_sys_cd_cgenerator)
            {
                string ov = db.singleDataString("select m_name from st_enq_cogenerators where m_sys_cd=" + psrc.f_sys_cd_cgenerator.ToString());
                string nv = db.singleDataString("select m_name from st_enq_cogenerators where m_sys_cd=" + f_sys_cd_cgenerator.ToString());
                sql = "Insert into t_enquiry_change_log (m_field_name,m_old_value, m_new_value,f_sys_cd_enq,m_stamp_add,m_actv,m_rstat) ";
                sql += " values ('Co-generator ','" + ov + "','" + nv + "'," + m_sys_cd.ToString() + ", getdate(),1,1);";
                db.executeSQL(sql, "");

            }
            if (psrc.f_sys_cd_cgenerator1 != f_sys_cd_cgenerator1)
            {
                string ov = db.singleDataString("select m_name from st_enq_cogenerators where m_sys_cd=" + psrc.f_sys_cd_cgenerator1.ToString());
                string nv = db.singleDataString("select m_name from st_enq_cogenerators where m_sys_cd=" + f_sys_cd_cgenerator1.ToString());
                sql = "Insert into t_enquiry_change_log (m_field_name,m_old_value, m_new_value,f_sys_cd_enq,m_stamp_add,m_actv,m_rstat) ";
                sql += " values ('Co-generator1 ','" + ov + "','" + nv + "'," + m_sys_cd.ToString() + ", getdate(),1,1);";
                db.executeSQL(sql, "");

            }

            if (psrc.nextfollowup != nextfollowup)
            {
                string ov = psrc.nextfollowup;
                string nv = nextfollowup;
                sql = "Insert into t_enquiry_change_log (m_field_name,m_old_value, m_new_value,f_sys_cd_enq,m_stamp_add,m_actv,m_rstat) ";
                sql += " values ('Next Follow-up Date ','" + ov + "','" + nv + "'," + m_sys_cd.ToString() + ", getdate(),1,1);";
                db.executeSQL(sql, "");
                usrlog = true;
            }

            if (psrc.plantobuydate != plantobuydate)
            {
                string ov = psrc.plantobuydate;
                string nv = plantobuydate;
                sql = "Insert into t_enquiry_change_log (m_field_name,m_old_value, m_new_value,f_sys_cd_enq,m_stamp_add,m_actv,m_rstat) ";
                sql += " values ('Plan to buy date ','" + ov + "','" + nv + "'," + m_sys_cd.ToString() + ", getdate(),1,1);";
                db.executeSQL(sql, "");
                usrlog = true;
            }


            if (psrc.f_actv_dt != f_actv_dt)
            {
                string ov = psrc.f_actv_dt;
                string nv = f_actv_dt;
                sql = "Insert into t_enquiry_change_log (m_field_name,m_old_value, m_new_value,f_sys_cd_enq,m_stamp_add,m_actv,m_rstat) ";
                sql += " values ('Activity date ','" + ov + "','" + nv + "'," + m_sys_cd.ToString() + ", getdate(),1,1);";
                db.executeSQL(sql, "");
                usrlog = true;
            }

            if (psrc.f_actv_cd != f_actv_cd)
            {
                string ov = db.singleDataString("select m_name from st_enq_cogenerators where m_sys_cd=" + psrc.f_actv_cd.ToString());
                string nv = db.singleDataString("select m_name from st_enq_cogenerators where m_sys_cd=" + f_actv_cd.ToString());
                if (String.IsNullOrEmpty(psrc.f_actv_cd)) ov = "No value";
                if (String.IsNullOrEmpty(f_actv_cd)) nv = "No value";

                sql = "Insert into t_enquiry_change_log (m_field_name,m_old_value, m_new_value,f_sys_cd_enq,m_stamp_add,m_actv,m_rstat) ";
                sql += " values ('Activity Value ','" + ov + "','" + nv + "'," + m_sys_cd.ToString() + ", getdate(),1,1);";

                db.executeSQL(sql, "");
                usrlog = true;

            }
            if (psrc.remark != remark)
            {
                string ov = psrc.remark;
                string nv = remark;
                sql = "Insert into t_enquiry_change_log (m_field_name,m_old_value, m_new_value,f_sys_cd_enq,m_stamp_add,m_actv,m_rstat) ";
                sql += " values ('Open Remark ','" + ov + "','" + nv + "'," + m_sys_cd.ToString() + ", getdate(),1,1);";
                db.executeSQL(sql, "");
                usrlog = true;
            }
            //if (psrc.productname != productname)
            //{
            //    string ov = psrc.productname;
            //    string nv = productname;
            //    sql = "Insert into t_enquiry_change_log (m_field_name,m_old_value, m_new_value,f_sys_cd_enq,m_stamp_add,m_actv,m_rstat) ";
            //    sql += " values ('Model interested ','" + ov + "','" + nv + "'," + m_sys_cd.ToString() + ", getdate(),1,1);";
            //    db.executeSQL(sql, "");
            //    usrlog = true;
            //}

            if (usrlog)
            {

                sql = "insert into t_Usr_enqlog(f_sys_cd_enq, m_Enquiry_dt, m_Followup_dt, m_PTB_dt, remark, m_stamp, m_actv, m_rstat)";
                sql += "values";
                sql += "(" + m_sys_cd + ", '" + enqdate + "', '" + nextfollowup + "', '" + plantobuydate + "', '" + remark + "', getdate(), 1, 1)";
                db.executeSQL(sql, "");
            }

        }

    }

    public class clsbookdata
    {

        public string book_dt { get; set; }
        public string book_model { get; set; }
        public string book_model_nm { get; set; }
        public int book_paymode { get; set; }
        public string book_finalprice { get; set; }
        public string book_amount { get; set; }
        public string book_remarks { get; set; }
        public string book_propdeldt { get; set; }
        public int book_model_purchased { get; set; }
        //public int chkradio { get; set; }


    }


    public class clsDelvdata
    {
        public string delv_dt { get; set; }
        public string pin_no { get; set; }
        public string delv_price { get; set; }
        public string delv_model { get; set; }
        public string delv_model_nm { get; set; }
        public string delv_actualprice { get; set; }

        public string delv_pname { get; set; }
        public string delv_remark { get; set; }
    }

    public class clsLost
    {
        public string lost_reason_cd { get; set; }
        public string lost_make_cd { get; set; }
        public string lost_modelpur { get; set; }
        public string lost_price { get; set; }
        public string lost_remarks { get; set; }
        public string lost_date { get; set; }
    }

    public class clsBasicInvent
    {
        public string model_cd { get; set; }
        public string year { get; set; }
        public string pinno { get; set; }
        public string rcno { get; set; }
        public int dlr_cd { get; set; }

    }

    #region supportclass
    public class clsListDataSupportA
    {

        public int optag { get; set; }
        public int stat_cd { get; set; }
        public string stat_nm { get; set; }
        public int sys_cd { get; set; }
        public string division { get; set; }
        public string dlr_state_nm { get; set; }
        public string tm_nm { get; set; }
        public string sap_cd { get; set; }
        public string dlr_nm { get; set; }
        public string location_nm { get; set; }
        public string cust_nm { get; set; }
        public string cust_mob_no { get; set; }
        public string cust_village { get; set; }
        public string cust_tehsil { get; set; }
        public string enq_id { get; set; }
        public string enq_dt { get; set; }
        public string enq_src { get; set; }
        public string model_interested { get; set; }
        public string enq_status { get; set; }
        public string dse_nm { get; set; }
        public string ptb_dt { get; set; }
        public string act_nm { get; set; }
        public string act_dt { get; set; }
        public string chkitm { get; set; }
    }
    public class clsListSupportA
    {

        public int status { get; set; }
        public string msg { get; set; }
        public List<clsListDataSupportA> data { get; set; }
    }

    #endregion
}