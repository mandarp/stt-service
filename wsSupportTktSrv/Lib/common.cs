﻿/* Common Bussiness Logic function LIB
 * 
 * 
 */

using System;
using System.Globalization;
using EMS.Lib;

namespace common_classes
{
    public class cmnResp
    {

        public int status { get; set; }
        public string msg { get; set; }
        public string sql { get; set; }
        public long ref_code { get; set; }
        public cmnResp()
        {
            status = 0; msg = "Init"; sql = ""; ref_code = 0;
        }

    }

    //45hu_20180814_1307
    public class clsResponse
    {
        public int status { get; set; }
        public string msg { get; set; }
        public string filename { get; set; }
        public int psz { get; set; }
    }

    public class clsCommon
    {

        public string gn(dynamic pVal)
        {

            if (pVal == null) return "NULL";
            else return Convert.ToString(pVal);

        }
        public static int GetNumber(String p_val) {
            int ret = 0;
            try {
                ret = Convert.ToInt32(p_val);
            } catch (Exception e) {

                ret = 0;
            }
            return ret;
        }

    }
    
}