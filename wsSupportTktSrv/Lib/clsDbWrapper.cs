﻿using common_classes;
using ems.Lib;
using ems.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;

namespace EMS.Lib
{
    public class clsDbWrapper
    {
        //Added By Shubh
        public string msg = "Ok";
        public bool col_esc = true;
        public string fieldlist = "";
        public string dbnull(int? p)
        {
            string ret = "";
            if (p == null) ret = "NULL";
            else ret = p.ToString();
            return ret;

        }
        public string dbnullQ(string p)
        {
            if (String.IsNullOrEmpty(p) || p.Length <= 0) return "NULL";
            else return "'" + p + "'";

        }
        //Ended By Shubh
        public cmnResp resp { get; set; }
        
        string Connstr = null;
        public SqlConnection con = null;
        public Resultset rs = null;
        public DataSet ldset = null;
        public bool rds = true;
        public string getASCII_str(String instr)
        {

            if (instr == null) return "";
            string ret = "";
            for (int i = 0; i < instr.Length; i++)
            {

                int j = Convert.ToInt32(instr[i]);
                if (j >= 32 && j <= 127) { ret += instr[i]; }
            }

            return ret;
        }
        public string cleanUnwanted(String instr)
        {

            if (instr == null) return "";
            string ret = "";
            for (int i = 0; i < instr.Length; i++)
            {

                int j = Convert.ToInt32(instr[i]);
                if (j >= 32 && j <= 127 && j != 39 && j != 34) { ret += instr[i]; }
            }

            return ret;
        }

        public clsDbWrapper()
        {
            resp = new cmnResp();
            // 103.221.68.206

             //Connstr = "Data source=103.221.68.206;database=SupTicSysDB;Uid=sa;pwd=mindspace@123;Persist Security Info=True;Connection TimeOut=50; Pooling=true;Max Pool Size=500;Min Pool Size=1;";// providerName = \"System.Data.SqlClient\""; 
            Connstr = "Data source=169.38.106.187;database=SupTicSysDB;Uid=sa;pwd=lX43gJJ7qjxF;Persist Security Info=True;Connection TimeOut=50; Pooling=true;Max Pool Size=500;Min Pool Size=1;";
            //Connstr = "Data Source = DESKTOP - 0GI3JOP; Initial Catalog = SysTicSysDB; Integrated Security = True";
        }

        public DataSet executeSQLDS(string p_SQL, string p_Reserved)
        {

            DataSet retVal = new DataSet();
            resp.ref_code = 0; resp.status = 1; resp.msg = "Ok";
            try
            {
                if (p_SQL.IndexOf("Select", StringComparison.OrdinalIgnoreCase) > -1 || p_SQL.IndexOf("Execute", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    CreateConn();

                    SqlDataAdapter da = new SqlDataAdapter();
                    SqlCommand cmd = con.CreateCommand();
                    cmd.CommandText = p_SQL;
                    cmd.CommandTimeout = 1200;
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(retVal);
                    CloseConn();
                }
                else if (p_SQL.IndexOf("Insert", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    CreateConn();
                    SqlCommand cmd = new SqlCommand(p_SQL, con);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = p_SQL;
                    var o = cmd.ExecuteScalar();
                    DataTable t = new DataTable("ret");
                    t.Columns.Add("code");
                    t.Rows.Add(o);
                    retVal.Tables.Add(t);
                    CloseConn();
                }
                else if (p_SQL.IndexOf("Update", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    CreateConn();
                    SqlCommand cmd = new SqlCommand(p_SQL, con);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = p_SQL;
                    int result = cmd.ExecuteNonQuery();
                    CloseConn();

                }
                else if (p_SQL.IndexOf("Delete", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    CreateConn();
                    SqlCommand cmd = new SqlCommand(p_SQL, con);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = p_SQL;
                    int result = cmd.ExecuteNonQuery();
                    CloseConn();

                }
            }
            catch (Exception ex)
            {
                resp.status = -1; resp.msg = ex.Message;
            }

            return retVal;
        }

        public string executeSQL(string p_SQL, string p_Reserved)
        {
            Resultset objres = new Resultset();
            ldset = null;
            objres.sql = p_SQL;
            string retVal = string.Empty;
            int result = 0;
            try
            {
                if (p_SQL.IndexOf("Insert", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    CreateConn();
                    SqlCommand cmd = new SqlCommand(p_SQL, con);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = p_SQL;
                    if (p_SQL.IndexOf("SCOPE_IDENTITY", StringComparison.OrdinalIgnoreCase) > -1)
                    {
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            reader.Read();
                            result = Convert.ToInt32(reader[0]);
                        }
                    }
                    else
                    {
                        result = cmd.ExecuteNonQuery();
                    }

                    objres.status = 200;
                    objres.msg = msg;
                    objres.refcode = result;
                    objres.rows = null;

                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                    MemoryStream ms = new MemoryStream();
                    serializer.WriteObject(ms, objres);
                    retVal = Encoding.UTF8.GetString(ms.ToArray());

                    CloseConn();
                }
                else if (p_SQL.IndexOf("Select", StringComparison.OrdinalIgnoreCase) > -1 || p_SQL.IndexOf("Execute", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    CreateConn();

                    SqlDataAdapter da = new SqlDataAdapter();
                    SqlCommand cmd = con.CreateCommand();
                    cmd.CommandText = p_SQL;
                    cmd.CommandTimeout = 300;
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    ldset = ds;
                    string[] jsonArray = new string[ds.Tables[0].Columns.Count];
                    string headString = string.Empty;
                    objres.rowsize = 0;
                    for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                    {
                        fieldlist += ds.Tables[0].Columns[i].Caption + "|";
                        int sz = ds.Tables[0].Columns[i].MaxLength;
                        if (sz < 0) { sz = 12;/*long number*/ }
                        objres.rowsize += sz;
                        jsonArray[i] = ds.Tables[0].Columns[i].Caption;
                        headString += "\"" + jsonArray[i] + "\" : \"" + jsonArray[i] + i.ToString() + "%" + "\",";
                        // headString += "\"" + jsonArray[i] + "\" : \"" + jsonArray[i] + i.ToString() + "%" + "\",";
                    }

                    headString = headString.Substring(0, headString.Length - 1);
                    StringBuilder sb = new StringBuilder();
                    sb.Append("[");
                    objres.trows = ds.Tables[0].Rows.Count;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            string colsString = headString;
                            sb.Append("{");
                            for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                            {
                                //                              colsString = colsString.Replace(ds.Tables[0].Columns[j] + j.ToString() + "%", ds.Tables[0].Rows[i][j].ToString());

                                String d = ds.Tables[0].Rows[i][j].ToString();
                                d = d.Replace("\\", "-");
                                d = d.Replace("\"", "\\\"");
                                d = d.Replace("\n", " ");
                                d = d.Replace("\r", " ");
                                if (col_esc) { d = d.Replace(":", " "); }
                          //      d = d.Replace("'", " ");
                           //     d = d.Replace('"', ' ');

                                d = getASCII_str(d);
                                colsString = colsString.Replace(ds.Tables[0].Columns[j] + j.ToString() + "%", d);
                            }
                            sb.Append(colsString + "},");
                        }
                    }
                    else
                    {
                        string colsString = headString;
                        sb.Append("{");
                        for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                        {
                            colsString = colsString.Replace(ds.Tables[0].Columns[j] + j.ToString() + "%", "");
                        }
                        sb.Append(colsString + "},");
                    }

                    sb = new StringBuilder(sb.ToString().Substring(0, sb.ToString().Length - 1));
                    sb.Append("]");

                    objres.status = 200;
                   
                    if (objres.trows == 0) {
                        msg = "No Records";
                    }
                        objres.refcode = -1;
                    objres.msg = msg;
                    objres.rows = sb.ToString();
                    rs = objres;
                    if (rds)
                    {
                        DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                        MemoryStream ms = new MemoryStream();
                        serializer.WriteObject(ms, objres);
                        retVal = Encoding.UTF8.GetString(ms.ToArray());
                    }
                    //DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                    //MemoryStream ms = new MemoryStream();
                    //serializer.WriteObject(ms, objres);
                    //retVal = Encoding.UTF8.GetString(ms.ToArray());

                    CloseConn();
                }
                else if (p_SQL.IndexOf("Update", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    CreateConn();
                    SqlCommand cmd = new SqlCommand(p_SQL, con);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = p_SQL;

                    result = cmd.ExecuteNonQuery();

                    objres.status = 200;
                    objres.msg = msg;
                    objres.refcode = result;
                    objres.rows = null;

                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                    MemoryStream ms = new MemoryStream();
                    serializer.WriteObject(ms, objres);
                    retVal = Encoding.UTF8.GetString(ms.ToArray());

                    CloseConn();
                }

                else if (p_SQL.IndexOf("Delete", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    CreateConn();
                    SqlCommand cmd = new SqlCommand(p_SQL, con);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = p_SQL;

                    result = cmd.ExecuteNonQuery();

                    objres.status = 200;
                    objres.msg = msg;
                    objres.refcode = result;
                    objres.rows = null;

                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                    MemoryStream ms = new MemoryStream();
                    serializer.WriteObject(ms, objres);
                    retVal = Encoding.UTF8.GetString(ms.ToArray());

                    CloseConn();
                }
            }
            catch (Exception ex)
            {
             //   Helper.WitreErrorLog(ex, p_SQL);
                objres.status = 0;
                objres.msg = ex.Message.ToString();
                objres.refcode = 0;
                objres.trows = 0;
                objres.rows = null;
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                MemoryStream ms = new MemoryStream();
                serializer.WriteObject(ms, objres);
                retVal = Encoding.UTF8.GetString(ms.ToArray());
            }

            return retVal;
        }
        public string jSelectSQL(string p_SQL, bool header, SqlConnection con1 = null)
        {
            Resultset objres = new Resultset();
            objres.sql = p_SQL;
            string retVal = string.Empty;
            int result = 0;
           // CreateConn(con1);

            try
            {

                if (p_SQL.IndexOf("Insert", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    objres.status = 404;
                    objres.msg = "Feature not available";
                    objres.refcode = result;
                    objres.rows = null;
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                    MemoryStream ms = new MemoryStream();
                    serializer.WriteObject(ms, objres);
                    retVal = Encoding.UTF8.GetString(ms.ToArray());
                }
                else if (p_SQL.IndexOf("Select", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    SqlDataAdapter da = new SqlDataAdapter();
                    SqlCommand cmd = con.CreateCommand();
                    cmd.CommandText = p_SQL;
                    cmd.CommandTimeout = 300;
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    DataSet ds2 = new DataSet();
                    da.FillSchema(ds, SchemaType.Mapped);
                    da.Fill(ds2);
                    string colString = string.Empty;
                    objres.rowsize = 0;
                    colString = "[ ";
                    //  int len = ds.Tables[0].Columns[0].;
                    for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                    {
                        int sz = ds.Tables[0].Columns[i].MaxLength;
                        if (sz < 0) { sz = 12;/*long number*/ }
                        objres.rowsize += sz;

                        colString = colString + "\"" + ds.Tables[0].Columns[i].Caption + "\"";
                        if (i + 1 < ds.Tables[0].Columns.Count) colString += ",";
                    }

                    colString += " ], ";

                    StringBuilder sb = new StringBuilder();
                    sb.Append("[");
                    if (header) sb.Append(colString);

                    objres.trows = ds2.Tables[0].Rows.Count;
                    string rowstring = "";
                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds2.Tables[0].Rows.Count; i++)
                        {
                            rowstring = "[";
                            for (int j = 0; j < ds2.Tables[0].Columns.Count; j++)
                            {
                                String d = ds2.Tables[0].Rows[i][j].ToString();
                                d = d.Replace("\\", "-");
                                d = d.Replace("\"", "\\\"");
                                d = d.Replace("\n", " ");
                                d = d.Replace("\r", " ");
                                d = getASCII_str(d);
                                rowstring += "\"" + d + "\"";
                                if (j + 1 < ds2.Tables[0].Columns.Count) rowstring += ",";
                            }
                            rowstring += "]";
                            if (i + 1 < ds2.Tables[0].Rows.Count) rowstring += ",";
                            sb.Append(rowstring);
                        }
                    }
                    //sb = new StringBuilder(sb.ToString().Substring(0, sb.ToString().Length - 1));
                    sb.Append("]");

                    objres.status = 200;
                    objres.msg = "Ok";
                    objres.refcode = -1;
                    objres.rows = sb.ToString();
                    rs = objres;
                    if (rds)
                    {
                        DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                        MemoryStream ms = new MemoryStream();
                        serializer.WriteObject(ms, objres);
                        retVal = Encoding.UTF8.GetString(ms.ToArray());
                    }
                }
                else if (p_SQL.IndexOf("Update", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    objres.status = 404;
                    objres.msg = "Feature not available";
                    objres.refcode = result;
                    objres.rows = null;
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                    MemoryStream ms = new MemoryStream();
                    serializer.WriteObject(ms, objres);
                    retVal = Encoding.UTF8.GetString(ms.ToArray());
                }
                else if (p_SQL.IndexOf("Delete", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    objres.status = 404;
                    objres.msg = "Feature not available";
                    objres.refcode = result;
                    objres.rows = null;
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                    MemoryStream ms = new MemoryStream();
                    serializer.WriteObject(ms, objres);
                    retVal = Encoding.UTF8.GetString(ms.ToArray());
                }
                if (con1 == null) { CloseConn(); }
            }
            catch (Exception ex)
            {
               // Helper.WitreErrorLog(ex, p_SQL);
                objres.status = 0;
                objres.msg = ex.Message.ToString();
                objres.refcode = 0;
                objres.trows = 0;
                objres.rows = null;
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                MemoryStream ms = new MemoryStream();
                serializer.WriteObject(ms, objres);
                retVal = Encoding.UTF8.GetString(ms.ToArray());
            }

            return retVal;
        }


        public DataTable getEnquiry_BySearch(int drl_cd, string enq_no, DateTime enq_frdate, DateTime enq_todate, int user_add, ref string err)
        {
            try
            {
                CreateConn();

                SqlDataAdapter da = new SqlDataAdapter();
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_GetEnquiry_BySearch";
                da.SelectCommand = cmd;
                cmd.Parameters.AddWithValue("@p_dlr_cd", drl_cd);
                cmd.Parameters.AddWithValue("@p_usr_id", user_add);
                if (string.IsNullOrEmpty(enq_no))
                    cmd.Parameters.AddWithValue("@p_enq_id", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@p_enq_id", enq_no);
                if (enq_frdate == DateTime.MinValue)
                    cmd.Parameters.AddWithValue("@p_fr_date", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@p_fr_date", enq_frdate);
                if (enq_todate == DateTime.MinValue)
                    cmd.Parameters.AddWithValue("@p_to_date", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@p_to_date", enq_todate);

                DataSet ds = new DataSet();
                da.Fill(ds);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
              //  Helper.WitreErrorLog(ex);
                err = ex.Message.ToString();
                return null;
            }
        }

        public string SendWebSMS(string f_enq_id, string msg, int user_add, int tag, ref string err)
        {
            string retVal = string.Empty;
            int result = 0;
            try
            {
                CreateConn();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_sendSMS_ToDealerDSE";

                cmd.Parameters.Add("@p_enq_id", SqlDbType.NVarChar).Value = f_enq_id;
                cmd.Parameters.Add("@p_msg", SqlDbType.NVarChar).Value = msg;
                cmd.Parameters.Add("@p_usr_cd", SqlDbType.Int).Value = user_add;
                cmd.Parameters.Add("@p_tag", SqlDbType.Int).Value = tag;

                result = cmd.ExecuteNonQuery();

                CloseConn();
            }
            catch (Exception ex)
            {
          //     Helper.WitreErrorLog(ex);
                err = ex.Message.ToString();
            }
            return retVal;
        }

        protected void CreateConn()
        {
            con = new SqlConnection(Connstr);
            con.Open();
        }

        public void CloseConn()
        {
            con.Close();
            con.Dispose();
        }

        public string executespquery(string p_SQL, string p_Reserved)
        {
            Resultset objres = new Resultset();
            objres.sql = p_SQL;
            string retVal = string.Empty;
#pragma warning disable CS0219 // The variable 'result' is assigned but its value is never used
            int result = 0;
#pragma warning restore CS0219 // The variable 'result' is assigned but its value is never used
            try
            {
                CreateConn();

                SqlDataAdapter da = new SqlDataAdapter();
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandText = p_SQL;
                cmd.CommandTimeout = 300;
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                string[] jsonArray = new string[ds.Tables[0].Columns.Count];
                string headString = string.Empty;

                for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                {
                    jsonArray[i] = ds.Tables[0].Columns[i].Caption;
                    // headString += "'" + jsonArray[i] + "' : '" + jsonArray[i] + i.ToString() + "%" + "',";
                    headString += "\"" + jsonArray[i] + "\" : \"" + jsonArray[i] + i.ToString() + "%" + "\",";
                }

                headString = headString.Substring(0, headString.Length - 1);
                StringBuilder sb = new StringBuilder();
                sb.Append("[");
                objres.trows = ds.Tables[0].Rows.Count;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string colsString = headString;
                        sb.Append("{");
                        for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                        {
                            string d = ds.Tables[0].Rows[i][j].ToString();
                            d = d.Replace("\\", "-");
                            d = d.Replace("\"", "\\\"");
                            d = d.Replace("\n", " ");
                            d = d.Replace("\r", " ");

                            colsString = colsString.Replace(ds.Tables[0].Columns[j] + j.ToString() + "%", d);
                        }
                        sb.Append(colsString + "},");
                    }
                }
                else
                {
                    string colsString = headString;
                    sb.Append("{");
                    for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                    {
                        colsString = colsString.Replace(ds.Tables[0].Columns[j] + j.ToString() + "%", "");
                    }
                    sb.Append(colsString + "},");
                }

                sb = new StringBuilder(sb.ToString().Substring(0, sb.ToString().Length - 1));
                sb.Append("]");

                objres.status = 200;
                objres.msg = "Ok";
                objres.refcode = -1;
                objres.rows = sb.ToString();

                DataContractJsonSerializer serializer = new DataContractJsonSerializer(objres.GetType());
                MemoryStream ms = new MemoryStream();
                serializer.WriteObject(ms, objres);
                retVal = Encoding.UTF8.GetString(ms.ToArray());

                CloseConn();

            }

            catch (Exception ex)
            {
              // Helper.WitreErrorLog(ex, p_SQL);
                objres.status = 0;
                objres.msg = ex.Message.ToString();
                objres.refcode = 0;
                objres.trows = 0;
                objres.rows = null;
            }

            return retVal;
        }


        public long singleDataCode(string p_sql)
        {
            long ret = -1;
            clsDbWrapper db = new clsDbWrapper();
            DataSet d;
            d = db.executeSQLDS(p_sql, "");
            if (d.Tables.Count > 0)
            {
                if (d.Tables[0].Rows.Count > 0)
                {
                    ret = Convert.ToInt32(d.Tables[0].Rows[0][0].ToString());
                }
            }
            db.CloseConn();
            return ret;
        }
        public string singleDataString(string p_sql)
        {
            string ret = "";
            clsDbWrapper db = new clsDbWrapper();
            DataSet d;
            d = db.executeSQLDS(p_sql, "");
            if (d.Tables.Count > 0)
            {
                if (d.Tables[0].Rows.Count > 0)
                {
                    ret = d.Tables[0].Rows[0][0].ToString().ToString();
                }
            }
            db.CloseConn();
            return ret;
        }

        public DataTable bindRole()
        {
            DataTable dt = new DataTable();
            Resultset objres = new Resultset();
            string retVal = string.Empty;
            try
            {
                CreateConn();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_UserMgmt";

                // cmd.Parameters.Add("@datatable", SqlDbType.Structured).Value = dt;
                //  cmd.Parameters.AddWithValue("@datatable", dt); // passing Datatable
                cmd.Parameters.AddWithValue("@action", 1); // passing Datatable
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                CloseConn();
            }
            catch (Exception ex)
            {
                objres.status = 0;
                objres.msg = ex.Message.ToString();
                objres.refcode = 0;
                objres.trows = 0;
                objres.rows = null;
            }
            return dt;
        }


        public DataTable bindDlrRole()
        {
            DataTable dt = new DataTable();
            Resultset objres = new Resultset();
            string retVal = string.Empty;
            try
            {
                CreateConn();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_DlrUserMgmt";

                // cmd.Parameters.Add("@datatable", SqlDbType.Structured).Value = dt;
                //  cmd.Parameters.AddWithValue("@datatable", dt); // passing Datatable
                cmd.Parameters.AddWithValue("@action", 1); // passing Datatable
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                CloseConn();
            }
            catch (Exception ex)
            {
                objres.status = 0;
                objres.msg = ex.Message.ToString();
                objres.refcode = 0;
                objres.trows = 0;
                objres.rows = null;
            }
            return dt;
        }

        public DataTable bindUsers()
        {
            DataTable dt = new DataTable();
            Resultset objres = new Resultset();
            string retVal = string.Empty;
            try
            {
                CreateConn();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_UserMgmt";

                // cmd.Parameters.Add("@datatable", SqlDbType.Structured).Value = dt;
                //  cmd.Parameters.AddWithValue("@datatable", dt); // passing Datatable
                cmd.Parameters.AddWithValue("@action", 6); // passing Datatable
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                CloseConn();
            }
            catch (Exception ex)
            {
                objres.status = 0;
                objres.msg = ex.Message.ToString();
                objres.refcode = 0;
                objres.trows = 0;
                objres.rows = null;
            }
            return dt;
        }

        public DataTable binddlrUsers(int dlr_cd,int f_role_cd=0, int p_branch_cd =0)
        {
            DataTable dt = new DataTable();
            Resultset objres = new Resultset();
            string retVal = string.Empty;
            try
            {
                CreateConn();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_DlrUserMgmt";

                // cmd.Parameters.Add("@datatable", SqlDbType.Structured).Value = dt;
                //  cmd.Parameters.AddWithValue("@datatable", dt); // passing Datatable
                cmd.Parameters.AddWithValue("@action", 6); // passing Datatable
                cmd.Parameters.AddWithValue("@f_dlr_cd", dlr_cd);
                cmd.Parameters.AddWithValue("@f_role_cd", f_role_cd);
                cmd.Parameters.AddWithValue("@p_branch_cd ", p_branch_cd);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                CloseConn();
            }
            catch (Exception ex)
            {
                objres.status = 0;
                objres.msg = ex.Message.ToString();
                objres.refcode = 0;
                objres.trows = 0;
                objres.rows = null;
            }
            return dt;
        }
        public DataTable binddlrUsersReportingTo(int dlr_cd, int f_role_cd = 0, int p_branch_cd = 0)
        {
            DataTable dt = new DataTable();
            Resultset objres = new Resultset();
            string retVal = string.Empty;
            try
            {
                CreateConn();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_DlrUserMgmt";

                // cmd.Parameters.Add("@datatable", SqlDbType.Structured).Value = dt;
                //  cmd.Parameters.AddWithValue("@datatable", dt); // passing Datatable
                cmd.Parameters.AddWithValue("@action", 7); // passing Datatable
                cmd.Parameters.AddWithValue("@f_dlr_cd", dlr_cd);
                cmd.Parameters.AddWithValue("@f_role_cd", f_role_cd);
                cmd.Parameters.AddWithValue("@p_branch_cd ", p_branch_cd);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                CloseConn();
            }
            catch (Exception ex)
            {
                objres.status = 0;
                objres.msg = ex.Message.ToString();
                objres.refcode = 0;
                objres.trows = 0;
                objres.rows = null;
            }
            return dt;
        }

        //By Shubh
        #region shubh
        public string SaveAppUser(string user_id, string dis_nam, string role_cd, string dlr_cd, string pwd, string dsg_cd, string contact, string email, string rights, int p_usr_cd)
        {
            Resultset objres = new Resultset();
            int result = 0;
            string retVal = string.Empty;
            try
            {
                CreateConn();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_UserMgmt";

                // cmd.Parameters.Add("@datatable", SqlDbType.Structured).Value = dt;
                cmd.Parameters.AddWithValue("@m_usr_id", user_id);
                cmd.Parameters.AddWithValue("@m_pwd", pwd);
                cmd.Parameters.AddWithValue("@m_display_name", dis_nam);
                cmd.Parameters.AddWithValue("@f_usr_add", p_usr_cd);



                if (String.IsNullOrEmpty(dsg_cd))
                {
                    cmd.Parameters.AddWithValue("@f_designation_cd", 0);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@f_designation_cd", Convert.ToInt32(dsg_cd));
                }

                cmd.Parameters.AddWithValue("@m_Contact_No", contact);
                if (String.IsNullOrEmpty(role_cd))
                {
                    cmd.Parameters.AddWithValue("@f_role_cd", 0);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@f_role_cd", Convert.ToInt32(role_cd));
                }
                if (String.IsNullOrEmpty(dlr_cd))
                {
                    cmd.Parameters.AddWithValue("@f_dlr_cd", 0);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@f_dlr_cd", Convert.ToInt32(dlr_cd));
                }

                cmd.Parameters.AddWithValue("@m_module_right_str", rights);
                cmd.Parameters.AddWithValue("@m_emailaddr", email);
                cmd.Parameters.AddWithValue("@action", 5);              
                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    retVal = "Action completed successfully.";
                }
                else
                {
                    retVal = "Action not completed successfully.";
                }

                CloseConn();
            }
            catch (Exception ex)
            {
                objres.status = 0;
                objres.msg = ex.Message.ToString();
                retVal = ex.Message.ToString();
                objres.refcode = 0;
                objres.trows = 0;
                objres.rows = null;
            }
            return retVal;
        }

        public string SaveDlrAppUser(string user_id, string dis_nam, string role_cd, string dlr_cd, string pwd, string dsg_cd, string contact, string email, string rights, int p_usr_cd, int p_RpttoUser)
        {
            Resultset objres = new Resultset();
            int result = 0;
            string retVal = string.Empty;
            try
            {
                CreateConn();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_save_mappUser";

                // cmd.Parameters.Add("@datatable", SqlDbType.Structured).Value = dt;
                cmd.Parameters.AddWithValue("@m_usr_id", user_id);
                cmd.Parameters.AddWithValue("@m_pwd", pwd);
                cmd.Parameters.AddWithValue("@m_display_name", dis_nam);
                cmd.Parameters.AddWithValue("@f_usr_add", p_usr_cd);



                if (String.IsNullOrEmpty(dsg_cd))
                {
                    cmd.Parameters.AddWithValue("@f_designation_cd", 0);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@f_designation_cd", Convert.ToInt32(dsg_cd));
                }

                cmd.Parameters.AddWithValue("@m_Contact_No", contact);
                if (String.IsNullOrEmpty(role_cd))
                {
                    cmd.Parameters.AddWithValue("@f_role_cd", 0);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@f_role_cd", Convert.ToInt32(role_cd));
                }
                if (String.IsNullOrEmpty(dlr_cd))
                {
                    cmd.Parameters.AddWithValue("@f_dlr_cd", 0);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@f_dlr_cd", Convert.ToInt32(dlr_cd));
                }

                cmd.Parameters.AddWithValue("@m_module_right_str", rights);
                cmd.Parameters.AddWithValue("@m_emailaddr", email);
                cmd.Parameters.AddWithValue("@f_sys_cd_parent", p_RpttoUser);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                if(ds.Tables.Count>0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                          result = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                        retVal = ds.Tables[0].Rows[0][1].ToString();
                    }
                }
                //result = Convert.ToInt32(cmd.ExecuteScalar());
                if (result > 0)
                {
                    retVal = "Action completed successfully.";
                }
            
                else
                {
                    retVal = "Action not completed successfully.";
                }

                CloseConn();
            }
            catch (Exception ex)
            {
                objres.status = 0;
                objres.msg = ex.Message.ToString();
                retVal = ex.Message.ToString();
                objres.refcode = 0;
                objres.trows = 0;
                objres.rows = null;
            }
            return retVal;
        }



        #endregion



        public string getExcelVirualPathfromDS(string p_dw_path, string p_fieldlist, string p_filename)
        {
            string downloadpath = "http://support.dealerdost.com/localfiles/";
            string path_Root = @"C:\iiswww\globalResource\xls";
            if (ldset.Tables.Count <= 0) { return "0 Table"; }
            if(ldset.Tables[0].Rows.Count <=0) { return "No open tickets found for this zone"; }

            string path_Random = Path.GetRandomFileName();
            string filepath = string.Empty; string fileName = string.Empty;
            fileName = path_Root + "\\" + path_Random + "\\" + p_dw_path + ".xlsx";
            ClosedXLHelper n = new ClosedXLHelper();
            //  n.colinfo = colinfo;
            String serverpath = downloadpath + path_Random + "/" + p_dw_path + ".xlsx"; // "http://ibm.mindspacetech.com/Grscxls/" + path_Random + "/" + dw_path+".xlsx";

            if(!String.IsNullOrEmpty(p_fieldlist)) { fieldlist = p_fieldlist; }
            n.AppendRow(fieldlist, fileName, "datasheet"); 
            n.AppendDS(ldset, fileName, "datasheet");
            return serverpath;
        }


    }

    public class Resultset
    {
        public int status { get; set; }
        public string msg { get; set; }
        public int refcode { get; set; }
        public int trows { get; set; }
        public int rowsize { get; set; }
        public string rows { get; set; }
        public string sql { get; set; }
        public string reserved { get; set; }


    }

    public class ResultsetEx : Resultset
    {
        public string p_offline_cd { get; set; }
    }

}