﻿using gen.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace backupSys.Lib
{
    class serviceHelper
    {

        public Boolean isServiceAvailable(string pServName)
        {
            ServiceController sc = new ServiceController();
            sc.ServiceName = pServName;
            try { string state = sc.Status.ToString(); }
            catch (Exception e) { return false; }
            return true;
        }


        public void startstop(bool toggle, string pServName)
        {

            try
            {
                ServiceController sc = new ServiceController();
                sc.ServiceName = pServName;
                if (toggle && sc.Status == ServiceControllerStatus.Stopped) {sc.Start();}
                if (!toggle && sc.Status == ServiceControllerStatus.Running) { sc.Stop(); }
            }
            catch (Exception ex)
            {
                logger log = new logger();
                log.logError("Error: " + ex.Message);

            }

        }
        public int getChildStatus(string pServName)
        {
            int ret = 0;
            ServiceController sc = new ServiceController();
            sc.ServiceName = pServName;
            string curservice_state = sc.Status.ToString();
 //           if(sc.Status == ServiceControllerStatus.PausePending)
             if (sc.Status.ToString().CompareTo("Running") == 0) ret = 1;
            if (sc.Status.ToString().CompareTo("StartPending") == 0) ret = 2;
            if (sc.Status.ToString().CompareTo("Stopped") == 0) ret = 3;
            if (sc.Status.ToString().CompareTo("StopPending") == 0) ret = 4;


            return ret;

        }
        public bool IsRunning(string pServName) {
            bool ret = false;
            ret = getSC(pServName).Status == ServiceControllerStatus.Running;
            return ret;
        }
        public bool IsStopped(string pServName)
        {
            bool ret = false;
            ret = getSC(pServName).Status == ServiceControllerStatus.Stopped;
            return ret;
        }


        public ServiceController getSC(string pServName)
        {
            ServiceController sc = new ServiceController();
            sc.ServiceName = pServName;
            return sc;

        }

        public string getChildStatusName(string childServiceName)
        {
            ServiceController sc = new ServiceController();
            sc.ServiceName = childServiceName;
            string curservice_state = sc.Status.ToString();
            return sc.Status.ToString();

        }

    }
}
