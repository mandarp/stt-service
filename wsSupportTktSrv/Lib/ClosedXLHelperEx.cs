﻿using ClosedXML.Excel;

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
namespace gen.Lib
{
    // Inline module

    public class clsColInfo
    {
        public string col_nm { get; set; }
        public string shw_col { get; set; }
        public string grp_on { get; set; }
        public string filtertxt { get; set; }
        public string fldnm { get; set; }
        public short grptag { get; set; }
        public string reserved { get; set; }


        public int col_type { get; set; }
        public int col_cat { get; set; }
        public int master_index { get; set; }

    }

    public class ClosedXLHelperEx
    {

        public List<clsColInfo> colinfo { get; set; }
        public ClosedXLHelperEx()
        {
            colinfo = null;

        }
        public clsColInfo getColByName(string p_name)
        {
            clsColInfo ret = null;
            if (colinfo == null) return null;
            foreach (var o in colinfo)
            {
                if (p_name == o.col_nm) { ret = o as clsColInfo; break; }

            }
            return ret;


        }

        public string setColInfo(string fldList)
        {
            string ret = "";
            int cc = 1;
            foreach (var o in fldList.Split('|'))
            {


                clsColInfo c = new clsColInfo();
                if (o.Split('^').Length == 3)
                {
                    c.col_nm = o.Split('^')[0];
                    c.fldnm = o.Split('^')[1];
                    ret += c.fldnm;
                    ret += "|";
                    string ctype = o.Split('^')[2];
                    if (ctype != null) { c.col_type = Convert.ToInt32(ctype); }
                    c.shw_col = "TRUE";
                    if (colinfo == null) colinfo = new List<clsColInfo>();
                    cc++;
                    colinfo.Add(c);
                }
            }

            ret = ret.Substring(0, ret.Length - 1);
            return ret;

        }
        private static bool HasWriteAccessToFile(string filePath)
        {
            try
            { File.GetAccessControl(filePath); return true; }
            catch (UnauthorizedAccessException) { return false; }
            catch (Exception e) { return false; }
        }
        bool CDIfNotExist(string pzDir) { bool exists = System.IO.Directory.Exists(pzDir); if (!exists) System.IO.Directory.CreateDirectory(pzDir); return exists; }
        public void OpenExcel(string path, string sheetname)
        {

        }
        bool ifStringDate(string p_in)
        {

            try { Convert.ToDateTime(p_in); }
            catch (Exception e) { return false; }

            return true;
        }

        public bool AppendDSEx(DataSet ds, string path, string sheetname, int table_no = 0)
        {
            var wb = new XLWorkbook();
            DataSet pdsc = ds;
            bool fpresent = false;
            if (colinfo == null) return false;
            if (colinfo.Count == 0) return false;
            // set header
            string fldlist = "";
            foreach (var o in colinfo) { fldlist += o.fldnm; fldlist += "|"; }



            string pathName = System.IO.Path.GetDirectoryName(path);

            // if (!HasWriteAccessToFile(pathName)) { return false; }
            bool ostate = CDIfNotExist(pathName);
            AppendRow(fldlist, path, sheetname);

            IXLWorksheet ws = null;
            if (!System.IO.File.Exists(path))
            {
                ws = wb.Worksheets.Add(sheetname);
            }
            else
            {
                wb = new XLWorkbook(path);
                fpresent = true;
                ws = wb.Worksheet(sheetname);
            }
            int c = 1;
            int r = 1;
            int ar = 1;
            ar = ws.Rows().Count() + 1;
            if (pdsc.Tables.Count > table_no)
            {



                for (r = 0; r < pdsc.Tables[table_no].Rows.Count; r++)
                {
                    c = 1;
                    for (int cr = 0; cr < pdsc.Tables[table_no].Columns.Count; cr++)
                    {

                        string data = pdsc.Tables[table_no].Rows[r][cr].ToString();
                        clsColInfo ci = getColByName(pdsc.Tables[table_no].Columns[cr].ColumnName);

                        if (ci != null)
                        {
                            if (ci.shw_col.ToUpper() == "TRUE")
                            {
                                int type = ci.col_type;
                                if (ci.col_type == -5)
                                {
                                    //  ws.Cell(ar, c).DataType = XLDataType.DateTime; ws.Cell(ar, c).Value = data; ws.Cell(ar, c).Style.DateFormat.Format = "dd/mm/yyyy";
                                }
                                else
                                {
                                    ws.Cell(ar, c).Value = data;
                                }
                                c++;
                            }
                        }

                    }
                    ar++;
                }

            }
            if (fpresent) wb.Save();
            else wb.SaveAs(path);
            return true;
        }

        public bool AppendDS(DataSet ds, string path, string sheetname)
        {
            var wb = new XLWorkbook();
            DataSet pdsc = ds;
            bool fpresent = false;
            string pathName = System.IO.Path.GetDirectoryName(path);
            // if (!HasWriteAccessToFile(pathName)) { return false; }
            bool ostate = CDIfNotExist(pathName);
            IXLWorksheet ws = null;
            if (!System.IO.File.Exists(path))
            {
                ws = wb.Worksheets.Add(sheetname);
            }
            else
            {
                wb = new XLWorkbook(path);
                fpresent = true;
                ws = wb.Worksheet(sheetname);
            }
            int c = 1;
            int r = 1;
            int ar = 1;
            ar = ws.Rows().Count() + 1;
            if (pdsc.Tables.Count > 0)
            {



                for (r = 0; r < pdsc.Tables[0].Rows.Count; r++)
                {
                    c = 1;
                    for (int cr = 0; cr < pdsc.Tables[0].Columns.Count; cr++)
                    {

                        string data = pdsc.Tables[0].Rows[r][cr].ToString();
                        clsColInfo ci = getColByName(pdsc.Tables[0].Columns[cr].ColumnName);
                        if (ci != null)
                        {
                            int type = ci.col_type;
                        }

                        if (ci != null && ci.col_type == -5)
                        {
                            //ws.Cell(ar, c).DataType = ClosedXML.Excel.XLDXLDataType.DateTime; ws.Cell(ar, c).Value = data; ws.Cell(ar, c).Style.DateFormat.Format = "dd/mm/yyyy";
                        }
                        else
                        {
                            ws.Cell(ar, c).Value = data;
                        }
                        c++;
                    }
                    ar++;
                }

            }
            if (fpresent) wb.Save();
            else wb.SaveAs(path);
            return true;
        }

        public bool AppendDSAdv(DataSet ds, string path, string sheetname)
        {
            try
            {

                var wb = new XLWorkbook();
                DataSet pdsc = ds;
                bool fpresent = false;
                string pathName = System.IO.Path.GetDirectoryName(path);
                // if (!HasWriteAccessToFile(pathName)) { return false; }
                bool ostate = CDIfNotExist(pathName);
                IXLWorksheet ws = null;
                if (!System.IO.File.Exists(path))
                {
                    ws = wb.Worksheets.Add(sheetname);
                }
                else
                {
                    wb = new XLWorkbook(path);
                    fpresent = true;
                    ws = wb.Worksheet(sheetname);
                }
                int c = 1;
                int r = 1;
                int ar = 1;
                ar = ws.Rows().Count() + 1;
                if (pdsc.Tables.Count > 0)
                {



                    for (r = 0; r < pdsc.Tables[0].Rows.Count; r++)
                    {
                        c = 1;
                        //      for (int cr = 0; cr < pdsc.Tables[0].Columns.Count; cr++)
                        foreach (var ci in colinfo)
                        {
                            bool allow = true;
                            string data = "";
                            try
                            {
                                data = pdsc.Tables[0].Rows[r][ci.col_nm].ToString();
                            }
                            catch (Exception e)
                            {
                                allow = false;
                                data = "";
                            }
                            //  if (allow)
                            {
                                int type = 1;
                                if (ci != null)
                                { type = ci.col_type; }
                                if (type == 1) { ws.Cell(ar, c).Value = data; }
                                if (type == 2) { ws.Cell(ar, c).Value = data; }
                                if (type == 3) { ws.Cell(ar, c).Value = data; ws.Cell(ar, c).Style.DateFormat.Format = "dd-MMM-yyyy"; }
                                //ws.Column(c).AdjustToContents();
                                c++;
                            }
                        }
                        ar++;
                    }

                }
                if (fpresent) wb.Save();
                else wb.SaveAs(path);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        public bool AppendRow(string psr, string path, string sheetname)
        {
            var wb = new XLWorkbook();
            bool fpresent = false;
            string pathName = System.IO.Path.GetDirectoryName(path);
            //if (!HasWriteAccessToFile(pathName))
            //{ return false; }
            bool ostate = CDIfNotExist(pathName);
            IXLWorksheet ws = null;
            if (!System.IO.File.Exists(path))
            {
                ws = wb.Worksheets.Add(sheetname);
            }
            else
            {
                wb = new XLWorkbook(path);
                fpresent = true;
                ws = wb.Worksheet(sheetname);
            }
            int c = 1;
            int r = ws.Rows().Count() + 1; 
            foreach (var o in psr.Split('|')) { ws.Cell(r, c).Value = o; c++; }


            if (fpresent) wb.Save();
            else wb.SaveAs(path);

            return true;

        }
        public bool AppendRowAdv(string psr, string path, string sheetname)
        {
            var wb = new XLWorkbook();
            bool fpresent = false;
            string pathName = System.IO.Path.GetDirectoryName(path);
            //if (!HasWriteAccessToFile(pathName))
            //{ return false; }
            bool ostate = CDIfNotExist(pathName);
            IXLWorksheet ws = null;
            if (!System.IO.File.Exists(path))
            {
                ws = wb.Worksheets.Add(sheetname);
            }
            else
            {
                wb = new XLWorkbook(path);
                fpresent = true;
                if(wb.Worksheets.FirstOrDefault(p => p.Name == sheetname) == null)
                {

                    ws = wb.Worksheets.Add(sheetname);
                }
                ws = wb.Worksheet(sheetname);
            }
            int c = 1;
            int r = ws.Rows().Count() + 1;
            foreach (var o in psr.Split('|'))
            {
                string val = "";
                string align = "left";
                String bcol = "#ffffff";
                string fcol = "#000000";
                string type = "1";
                string brcol = "#000000";

                if (o.Split('^').Length > 0) { val = o.Split('^')[0]; }
                if (o.Split('^').Length > 1) { align = o.Split('^')[1]; }
                if (o.Split('^').Length > 2) { type = o.Split('^')[2]; }
                if (o.Split('^').Length > 3) { fcol = o.Split('^')[3]; }
                if (o.Split('^').Length > 4) { bcol = o.Split('^')[4]; }

                if (String.IsNullOrEmpty(val)) { val = " "; }

                ws.Cell(r, c).Value = val;
                ws.Cell(r, c).Style.Font.FontSize = 12;
                ws.Cell(r, c).Style.Fill.SetBackgroundColor(XLColor.FromHtml(bcol));
                ws.Cell(r, c).Style.Border.OutsideBorder= XLBorderStyleValues.Thin;

                ws.Cell(r, c).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                if (align.ToLower().CompareTo("left") == 0)
                {
                    ws.Cell(r, c).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                }
                if (align.ToLower().CompareTo("right") == 0)
                {
                    ws.Cell(r, c).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                }


                ws.Cell(r, c).Style.Alignment.Vertical= XLAlignmentVerticalValues.Center;


                ws.Cell(r, c).Style.Font.FontColor=XLColor.FromHtml(fcol);
                ws.Cell(r, c).Style.Border.SetLeftBorderColor( (XLColor.FromHtml(brcol)));

                if (type == "3") { ws.Cell(r, c).Style.DateFormat.Format = "dd-MMM-yyyy"; }

                c++;

            }


            if (fpresent) wb.Save();
            else wb.SaveAs(path);

            return true;

        }

        public bool AppendDSAdv(DataSet ds, string path, string sheetname, int table_index = 0)
        {
            try
            {

                var wb = new XLWorkbook();
                DataSet pdsc = ds;
                bool fpresent = false;
                string pathName = System.IO.Path.GetDirectoryName(path);
                // if (!HasWriteAccessToFile(pathName)) { return false; }
                bool ostate = CDIfNotExist(pathName);
                IXLWorksheet ws = null;
                if (!System.IO.File.Exists(path))
                {
                    ws = wb.Worksheets.Add(sheetname);
                }
                else
                {
                    wb = new XLWorkbook(path);
                    fpresent = true;
                    ws = wb.Worksheet(sheetname);
                }
                int c = 1;
                int r = 1;
                int ar = 1;
                ar = ws.Rows().Count() + 1;
                if (pdsc.Tables.Count > table_index)
                {



                    for (r = 0; r < pdsc.Tables[table_index].Rows.Count; r++)
                    {
                        c = 1;
                        //      for (int cr = 0; cr < pdsc.Tables[0].Columns.Count; cr++)
                        foreach (var ci in colinfo)
                        {
                            bool allow = true;
                            string data = "";
                            try
                            {
                                data = pdsc.Tables[table_index].Rows[r][ci.col_nm].ToString();
                            }
                            catch (Exception e)
                            {
                                allow = false;
                                data = "";
                            }
                            ws.Cell(ar, c).Value = data;
                            c++;
                            ////  if (allow)
                            //{
                            //    int type = 1;
                            //    if (ci != null)
                            //    { type = ci.col_type; }
                            //    if (type == 1) { ws.Cell(ar, c).Value = data; }
                            //    if (type == 2) { ws.Cell(ar, c).Value = data; }
                            //    if (type == 3) { ws.Cell(ar, c).Value = data; ws.Cell(ar, c).Style.DateFormat.Format = "dd-MMM-yyyy"; }
                            //    //ws.Column(c).AdjustToContents();
                            //    c++;
                            //}
                        }
                        ar++;
                    }

                }
                if (fpresent) wb.Save();
                else wb.SaveAs(path);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }
    }
}