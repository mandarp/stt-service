﻿using ClosedXML.Excel;
using ems.Models.EMS;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace ems.Lib
{
    public class ClosedXLHelper
    {
       
        public List<clsColInfo> colinfo { get; set; }
        public ClosedXLHelper() {
            colinfo = null;

        }
        public clsColInfo getColByName(string p_name) {
            clsColInfo ret = null;
            if (colinfo == null) return null;
            foreach(var o in colinfo)
            {
                if (p_name == o.col_nm) { ret = o as clsColInfo; break; }
           
            }
            return ret;


        }
        private static bool HasWriteAccessToFile(string filePath)
        {
            try
            { File.GetAccessControl(filePath); return true; }
            catch (UnauthorizedAccessException) { return false; }
            catch (Exception e) { return false; }
        }
        bool CDIfNotExist(string pzDir) { bool exists = System.IO.Directory.Exists(pzDir); if (!exists) System.IO.Directory.CreateDirectory(pzDir); return exists; }
        public void OpenExcel(string path,string sheetname) {
                     
        }
        bool ifStringDate(string p_in) {

            try { Convert.ToDateTime(p_in); }
            catch (Exception e) { return false; }

            return true;
        }


        public bool AppendDS(DataSet ds, string path,string sheetname)
        {
            var wb = new XLWorkbook();
            DataSet pdsc = ds;
            bool fpresent = false;
            string pathName = System.IO.Path.GetDirectoryName(path);
           // if (!HasWriteAccessToFile(pathName)) { return false; }
            bool ostate = CDIfNotExist(pathName);
            IXLWorksheet ws = null;
            if (!System.IO.File.Exists(path))
            {
                ws = wb.Worksheets.Add(sheetname);
            }
            else
            {
                wb = new XLWorkbook(path);
                fpresent = true;
                ws = wb.Worksheet(sheetname);
            }
            int c = 1;
            int r = 1;
            int ar = 1;
            ar = ws.Rows().Count()+1;
            if (pdsc.Tables.Count > 0)
            {
                


                for ( r = 0; r <  pdsc.Tables[0].Rows.Count; r++)
                {
                    c = 1;
                    for (int cr = 0; cr < pdsc.Tables[0].Columns.Count; cr++)
                    {

                        string data = pdsc.Tables[0].Rows[r][cr].ToString();
                        clsColInfo ci = getColByName(pdsc.Tables[0].Columns[cr].ColumnName);
                        if (ci != null)
                        {
                            int type = ci.col_type;
                        }
                       
                        if (ci != null && ci.col_type == -5)
                        {
                            ws.Cell(ar, c).DataType = XLDataType.DateTime; ws.Cell(ar, c).Value = data; ws.Cell(ar, c).Style.DateFormat.Format = "dd/mm/yyyy";
                        }
                        else
                        {
                            ws.Cell(ar, c).Value = data;
                        }
                        c++;
                    }
                    ar++;
                }

            }
            if (fpresent) wb.Save();
            else wb.SaveAs(path);
            return true;
        }
        public bool AppendRow(string psr, string path, string sheetname)
        {
            var wb = new XLWorkbook();
            bool fpresent = false;
            string pathName = System.IO.Path.GetDirectoryName(path);
            //if (!HasWriteAccessToFile(pathName))
            //{ return false; }
            bool ostate = CDIfNotExist(pathName);
            IXLWorksheet ws =null;
            if (!System.IO.File.Exists(path))
            {
                ws = wb.Worksheets.Add(sheetname);
            }
            else
            {
                wb = new XLWorkbook(path);
                fpresent = true;
                ws=wb.Worksheet(sheetname);
            }
            int c = 1;
            int r = 1;
            foreach (var o in psr.Split('|')) {  ws.Cell(r, c).Value = o;c++;}

            if (fpresent) wb.Save();
            else wb.SaveAs(path);

            return true;

        }


    }
}